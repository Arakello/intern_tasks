#!/usr/bin/env python3

import sys
import pycountry

def main():
  """
  script's main function
  :returns: True if executed correctly and False if cannot open file or file path is wrong
  """
  file_path = get_input_path()

  try:
    with open(file_path,'r',1,'utf-8') as input_file:
      interpreted_contet = read_file(input_file)
  except UnicodeDecodeError:
    with open(file_path,'r',1,'utf-16') as input_file:
      interpreted_contet = read_file(input_file)
  except FileNotFoundError:
    sys.stderr.write("Error cannot open file!\n")
    return False

  # Sorts data twice first time by country, second by date.
  sorted_content = sort_output(sort_output(interpreted_contet,'country'),'date')
  
  formated_output = format_output(sorted_content)
  output_path = define_output_path(file_path)
  return write_output_to_file(output_path,formated_output)


def get_input_path():
  """
  :file_path: gets file path from first command-line argument or reads it with build-in input function
  """
  if len(sys.argv) > 1:
    file_path = sys.argv[1]
  else:
    file_path = input('File path: ')
  return file_path


def read_file(input_file):
  """
  reads file per line and interprets them

  :file_content: list containing parsed content of input file
  :redundancy_dict: dictionary containing countries and dates that has appered in input file before
  :index_changed: flag that determinates if index of file_content list has changed, by deafult is set as True
  """
  file_content = []
  redundancy_dict = {}
  index = 0 
  for row in input_file:
    
    index_changed = True
    
    row = remove_line_endings(row)
    row = row.split(',')
    row_content = interprete_row_content(row)
    
    # Skips iteration when error occured in interprete_row_content function
    if row_content:
      current_date = row_content['date']
      current_country = row_content['country']
        
      if is_redundant(redundancy_dict, current_country, current_date):
        redundand_entry = redundancy_dict[current_country][current_date]
        file_content[redundand_entry] = aggregate_rows(file_content[redundand_entry], row_content)
        index_changed = False
      else:
        redundancy_dict[current_country] = { current_date: index }

      if index_changed:
        file_content.append(row_content)
        index += 1 
  return file_content


def remove_line_endings(input_content):
  if "\r\n" in input_content:
    wihout_line_endins = input_content.replace("\r\n", "")
  elif "\r" in input_content:
    wihout_line_endins = input_content.replace("\r", "")
  elif "\n" in input_content:
    wihout_line_endins = input_content.replace("\n", "")
  return wihout_line_endins


def interprete_row_content(content):
  """
  takes list of row elements as an argument and interprets them 
  :content: list containing row elements in order date, state, impressions, ctr
  :returns: dictionary of elements expected by script output 
            if ValueError or IndexError is rised prints error message and returns false
  """
  try:
    date = change_date_format(content[0])
    country = change_state_to_country(content[1])
    impressions = int(content[2])
    ctr = get_ctr(content[3])
    clicks = calculate_clicks(impressions, ctr)
  except (ValueError,IndexError):
    sys.stderr.write("Error wrong data format!\n")
    return False
  output_row = { "date": date, "country": country, "impressions": impressions, "clicks": clicks }
  return output_row


def change_date_format(input_date):
  """
  changes date in mm/dd/yyyy format to yyyy-mm-dd
  """
  mdy_list = input_date.split("/")
  formated_date = mdy_list[2] + "-" + mdy_list[0] + "-" + mdy_list[1]
  return formated_date


def change_state_to_country(state):
  """
  uses pycountry to get three letter county code form given state
  for unknown states LookupError is caught and function returns XXX as a county code
  """
  try:
    return pycountry.subdivisions.lookup(state).country.alpha_3
  except LookupError:
    sys.stderr.write("Couldn't find country for %(state)s.\n" % {'state': state})
    return 'XXX'


def get_ctr(ctr):
  """
  converts ctr form string to float
  if ValueError is rised it catches it and handles it by 
  removing last character from string that is probably a % sign
  """
  try:
    return float(ctr)/100
  except ValueError:
    return float(ctr[:-1])/100


def calculate_clicks(impressions, ctr):
  return round(impressions * ctr)


def is_redundant(previous, current_country, current_date):
  return current_country in previous and current_date in previous[current_country]


def aggregate_rows(existing_row, new_rows):
  """
  adds impressions and clicks of two rows  
  """
  existing_row['impressions'] = existing_row['impressions'] + new_rows['impressions']
  existing_row['clicks'] = existing_row['clicks'] + new_rows['clicks']
  return existing_row


def sort_output(presorted_data,sort_key):
  return sorted(presorted_data, key=lambda k: k[sort_key]) 


def format_output(csv_content):
  """
  gets content of dictionary and transforms it to CSV string  
  """
  output_string = ''
  for line in csv_content:
      output_string += line['date'] + ',' + line['country'] + ',' + str(line['impressions']) + ',' + str(line['clicks']) + '\n'
  return output_string


def define_output_path(path):
  """
  creates output path based on path to input file
  finds the last '.' in input path and adds '_output' before it
  if there is no '.' which means file has no extension it simply adds '_output' at the end
  """
  extension_dot = path.rfind('.')
  if extension_dot == -1:
    output_path = path + '_output'
  else:
    output_path = path[:extension_dot] + '_output' + path[extension_dot:]
  return output_path


def write_output_to_file(path,output):
  output_file = open(path, 'w', 1, 'utf-8')  
  output_file.write(output)
  output_file.close()
  return path


if __name__ == "__main__":
    main()
