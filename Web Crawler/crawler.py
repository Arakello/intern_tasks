import urllib.request
import re
import sys

def site_map(url):
  """
  takes a site URL as an argument and creates a mapping of that 
  domain as a Python dictionary

  :map_of_site: main dictionary containing site's map
  :visited_pages: list of already visited pages
  :found_pages: list of all pages found in site
  :page_map: dictionary with two key: value pairs,
             first pair is title of given page with key 'title',
             second pair is set of links found in that page with key 'links'
  :new_links: list of links found in given page
  """
  map_of_site = {}
  visited_pages = []
  found_pages = [url]
  for page_url in found_pages:
    if page_url not in visited_pages:
      page_map = get_page_map(page_url,url)
      if page_map:
        map_of_site[page_url] = page_map
        new_links = list(map_of_site[page_url]['links'])
        found_pages += new_links
      visited_pages.append(page_url)
  return map_of_site


def get_page_map(url,root_url):
  """
  builds page_map

  :returns: page_map as a dictionary if page_content is accessed correctly
            or false when not
  """
  page_content = get_page_content(url)
  if page_content:
    title = get_title(page_content[0])
    links = get_links(page_content[1],root_url)
    page_map = { 'title': title, 'links': links }
  else:
    page_map = page_content
  return page_map


def get_page_content(url):
  """
  gets HTML from url and splits it in two parts, header and body

  :returns: HTML in two parts if url is correct or false when url is incorrect
  """
  try:
    page = urllib.request.urlopen(url)
    page_content = page.read().decode('utf-8').split('</head>',1)
    page.close()
    return page_content
  except ValueError:
    sys.stderr.write("Wrong link %s!\n" % url)
    return False


def get_title(page_content):
  """
  uses regular expression to get title of given page

  :returns: title written in <title> tag
            when there is no <title> tag returns empty string
  """
  title_regex = '(?<=\<title\>).*(?=\<\/title\>)'
  try:
    page_title = re.search(title_regex, page_content).group(0)
  except AttributeError:
    page_title = ""
  return page_title

def get_links(page_content, root_url):
  """
  uses regular expressions to get links present in given page

  :returns: set of links found in page
  """
  full_link_regex = '(?<=href\="%s).*?(?=")' % root_url
  shorter_link_regex = '(?<=href\="\/).*?(?=")'
  full_path_links = re.findall(full_link_regex, page_content)
  shorter_path_links = re.findall(shorter_link_regex, page_content)
  full_path_links = [ root_url + link for link in full_path_links ]
  shorter_path_links = [ root_url + '/' + link for link in shorter_path_links ]
  links = full_path_links + shorter_path_links
  links_set = set(links)
  return links_set
