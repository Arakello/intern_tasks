#README

## Task 1 - CSV Report Processing

Solution is located at 'CSV Report Processing' directory.

Works with Python 3.7.1.

For script to work properly pycountry library is needed.
To install required library run:


```pip install -r requirements.txt```

in 'CSV Report Processing' directory.

To run a script in 'CSV Report Processing' directory use: ``` ./script.py ```

If you do so sript should ask you to type path to the input file.
You can also pass path to the input file as an command line argument like: ``` ./script.py  <file_name> ```

Either way is fine.
Script's output is a file named ```<input_file_name>_output.<input_extension>```. The file should be created in input file's directory.

## Task 1 - Web Crawler

Solution is located at 'Web Crawler' directory in file 'crawler.py'.

Works with Python 3.7.1.

To run solution import site_map function from crawler.py file and call it with URL as an argument. 
For example type:

```
from crawler import site_map
site_map('<url>')
```
in python command line.

Function should return map of specified domain as a Python dictionary.
